﻿class DoubleShiftModel {
	constructor(textInput) {
		if (textInput == undefined)
			textInput = "";

		this.GridSize = this.calcGridSize(textInput);
		this.TextInput = this._fixTextInput(textInput);

		this.KeysX = this.generateKeys();
		this.KeysY = this.generateKeys();

		this.Answer = this.calcAnswer();
		this.ViewTable = this.generateViewTable(textInput, this.KeysX, this.KeysY);
	}

	_swapRowsInText(text, indx0, indx1) {
		let rows = chunkString(text, this.calcGridSize());

		const buf = rows[indx0];
		rows[indx0] = rows[indx1];
		rows[indx1] = buf;

		return rows.join("");
	}

	_swapColumnsInText(text, indx0, indx1) {
		let rows = chunkString(text, this.calcGridSize());

		for (let i = 0; i < this.calcGridSize(); i++) {
			const buf = rows[i][indx0];
			rows[i] = rows[i].replaceAt(indx0, rows[i][indx1]);
			rows[i] = rows[i].replaceAt(indx1, buf);
		}

		return rows.join("");
	}

	_fixTextInput(text) {
		this.GridSize = this.calcGridSize(text);
		for (let i = 0; i < (this.GridSize * this.GridSize); i++) {
			if (text[i] == undefined)
				text += " ";
		}
		return text;
	}

	calcGridSize(text) {
		if (text == undefined) {
			text = this.TextInput;
		}

		if (text == undefined) {
			text = "";
		}

		return Math.ceil(Math.sqrt(text.length));
	}

	generateKeys() {
		let keys = [];

		for (let i = this.calcGridSize(); i > 0; i--) {
			keys.push(i);
			keys = this.shuffle(keys);
		}

		return keys;
	}

	generateTable(text) {
		let table = [];
		const gridSize = this.calcGridSize(text);

		for (let i = 0; i < gridSize; i++) {
			table[i] = [];
			for (let x = 0; x < gridSize; x++) {
				if (text[i * gridSize + x] == undefined) {
					table[i][x] = " ";
				} else {
					table[i][x] = text[i * gridSize + x].toString();
				}
			}
		}

		return table;
	}

	generateViewTable(text, keysX, keysY) {

		if (text == undefined || keysX == undefined || keysY == undefined)
			throw new Error("generateViewTable wrong prameters");

		let viewSize = this.calcGridSize(text) + 1;

		let viewTable = [];

		// set array of arrays [viewTableSize X viewTableSize]
		for (let i = 0; i < viewSize; i++) {
			viewTable[i] = [];
		}

		// set keysX to viewTable
		viewTable[0][0] = undefined;
		for (let i = 1; i < viewSize; i++) {
			viewTable[0][i] = keysX[i - 1];
		}

		// set keysY to viewTable
		for (let i = 1; i < viewSize; i++) {
			viewTable[i][0] = keysY[i - 1];
		}

		// set textInput to viewTable
		let textCar = 0;

		for (let y = 1; y < viewSize; y++) {
			for (let x = 1; x < viewSize; x++) {
				viewTable[y][x] = text[textCar++];
			}
		}

		return viewTable;
	}

	calcAnswer(keysX, keysY) {
		let tmpTable = this.generateTable(this.TextInput);
		let lineLength = this.calcGridSize(this.TextInput);

		if (keysX == undefined) {
			keysX = [...this.KeysX];
		}
		if (keysY == undefined) {
			keysY = [...this.KeysY];
		}

		for (let l = 0; l < lineLength; l++) {
			for (let r = 0; r < lineLength - l - 1; r++) {

				if (keysX[r] > keysX[r + 1]) {

					for (let y = 0; y < lineLength; y++) {
						const buf = tmpTable[y][r];
						tmpTable[y][r] = tmpTable[y][r + 1];
						tmpTable[y][r + 1] = buf;

					}

					const buf = keysX[r];
					keysX[r] = keysX[r + 1];
					keysX[r + 1] = buf;
				}
			}
		}

		for (let l = 0; l < lineLength; l++) {
			for (let r = 0; r < lineLength - l - 1; r++) {

				if (keysY[r] > keysY[r + 1]) {

					for (let x = 0; x < lineLength; x++) {
						const buf = tmpTable[r][x];
						tmpTable[r][x] = tmpTable[r + 1][x];
						tmpTable[r + 1][x] = buf;

					}

					const buf = keysY[r];
					keysY[r] = keysY[r + 1];
					keysY[r + 1] = buf;
				}
			}
		}

		let answer = "";

		for (let i = 0; i < lineLength; i++) {
			for (let x = 0; x < lineLength; x++) {
				answer += tmpTable[i][x].toString();
			}
		}

		return answer.replace(new RegExp(" ", 'g'), "_");
	}

	_getRightKeyOrder(gridSize) {
		if (gridSize == undefined) {
			gridSize = this.calcGridSize(this.TextInput);
		}
		let keys = [];

		for (let i = 0; i < gridSize; i++) {
			keys[i] = i + 1;
		}

		return keys;
	}

	calcAnswerSourceText(keysXneed, keysYneed) {
		let lineLength = this.calcGridSize(this.TextInput);

		if (keysXneed == undefined) {
			keysXneed = [...this.KeysX];
		}
		if (keysYneed == undefined) {
			keysYneed = [...this.KeysY];
		}

		let keysXcur = this._getRightKeyOrder();
		let keysYcur = this._getRightKeyOrder();
		let textAns = this.TextInput;

		// columns
		for (let car = 0; car < lineLength; car++) {

			if (keysXneed[car] === keysXcur[car]) {
				continue;
			}

			const indxRightPosition = keysXcur.indexOf(keysXneed[car]);
			textAns = this._swapColumnsInText(textAns, car, indxRightPosition);

			const buf = keysXcur[car];
			keysXcur[car] = keysXcur[indxRightPosition];
			keysXcur[indxRightPosition] = buf;
		}

		// rows
		for (let car = 0; car < lineLength; car++) {

			if (keysYneed[car] === keysYcur[car]) {
				continue;
			}

			const indxRightPosition = keysYcur.indexOf(keysYneed[car]);
			textAns = this._swapRowsInText(textAns, car, indxRightPosition);

			const buf = keysYcur[car];
			keysYcur[car] = keysYcur[indxRightPosition];
			keysYcur[indxRightPosition] = buf;
		}

		return textAns.replace(new RegExp(" ", 'g'), "_");;
	}

	getAnimationQueue() {
		let animationQueue = [];
		let max = this.KeysX.length;
		let textAns = this._fixTextInput(this.TextInput);

		let keysX = [...this.KeysX];
		let keysY = [...this.KeysY];

		const viewSlide = this.generateViewTable(textAns, keysX, keysY);
		const animationElement = new AnimationQueueElement(viewSlide);
		animationQueue.push(animationElement);

		for (let l = 0; l < max; l++) {
			for (let r = 0; r < max - l - 1; r++) {
				if (keysX[r] > keysX[r + 1]) {
					// swap in text
					textAns = this._swapColumnsInText(textAns, r, r + 1);

					// swap keys
					const buf = keysX[r];
					keysX[r] = keysX[r + 1];
					keysX[r + 1] = buf;

					// create animation ViewTable
					const viewSlide = this.generateViewTable(textAns, keysX, keysY);
					const animationElement = new AnimationQueueElement(viewSlide, r + 1, r + 2, false);
					animationQueue.push(animationElement);
				}
			}
		}

		for (let l = 0; l < max; l++) {
			for (let r = 0; r < max - l - 1; r++) {
				if (keysY[r] > keysY[r + 1]) {
					// swap in text
					textAns = this._swapRowsInText(textAns, r, r + 1);

					// swap keys
					const buf = keysY[r];
					keysY[r] = keysY[r + 1];
					keysY[r + 1] = buf;

					// create animation ViewTable
					const viewSlide = this.generateViewTable(textAns, keysX, keysY);
					const animationElement = new AnimationQueueElement(viewSlide, r + 1, r + 2, true);
					animationQueue.push(animationElement);
				}
			}
		}

		return animationQueue;
	}

	getAnimationQueueSourceText() {
		let animationQueue = [];
		let lineLength = this.calcGridSize(this.TextInput);

		const keysXneed = [...this.KeysX];
		const keysYneed = [...this.KeysY];


		let keysXcur = this._getRightKeyOrder();
		let keysYcur = this._getRightKeyOrder();
		let textAns = this.TextInput;

		// slide #0
		const viewSlide = this.generateViewTable(textAns, keysXcur, keysYcur);
		const animationElement = new AnimationQueueElement(viewSlide);
		animationQueue.push(animationElement);

		// columns
		for (let car = 0; car < lineLength; car++) {

			if (keysXneed[car] === keysXcur[car]) {
				continue;
			}

			const indxRightPosition = keysXcur.indexOf(keysXneed[car]);
			textAns = this._swapColumnsInText(textAns, car, indxRightPosition);

			const buf = keysXcur[car];
			keysXcur[car] = keysXcur[indxRightPosition];
			keysXcur[indxRightPosition] = buf;

			// create animation ViewTable
			const viewSlide = this.generateViewTable(textAns, keysXcur, keysYcur);
			const animationElement = new AnimationQueueElement(viewSlide, car+1, indxRightPosition+1, false);
			animationQueue.push(animationElement);
		}

		// rows
		for (let car = 0; car < lineLength; car++) {

			if (keysYneed[car] === keysYcur[car]) {
				continue;
			}

			const indxRightPosition = keysYcur.indexOf(keysYneed[car]);
			textAns = this._swapRowsInText(textAns, car, indxRightPosition);

			const buf = keysYcur[car];
			keysYcur[car] = keysYcur[indxRightPosition];
			keysYcur[indxRightPosition] = buf;

			// create animation ViewTable
			const viewSlide = this.generateViewTable(textAns, keysXcur, keysYcur);
			const animationElement = new AnimationQueueElement(viewSlide, car+1, indxRightPosition+1, true);
			animationQueue.push(animationElement);
		}

		return animationQueue;
	}

	shuffle(array) {
		var currentIndex = array.length, temporaryValue, randomIndex;

		while (0 !== currentIndex) {
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}
}