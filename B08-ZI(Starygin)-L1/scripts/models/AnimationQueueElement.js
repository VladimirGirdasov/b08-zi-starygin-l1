﻿class AnimationQueueElement {
	constructor (viewTableSlide, cellAxisIndexA, cellAxisIndexB, boolHorizontalAxis) {
		this.ViewTableSlide = viewTableSlide;
		this.CellAxisIndexA = cellAxisIndexA;
		this.CellAxisIndexB = cellAxisIndexB;
		this.BoolHorizontalAxis = boolHorizontalAxis;
	}
}