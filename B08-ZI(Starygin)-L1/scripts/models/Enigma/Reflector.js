﻿class Reflector {
	// alphabetPairs - ожидает массив(26) символов ключей на который отражается i-й символ англ. алфавита
	constructor(name, alphabetPairs) {
		this.Name = name.toString();

		if (alphabetPairs.length !== 26) {
			throw Error("Классический рефлектор должен состоять из 26 символов");
		}

		this.AlphabetPairs = alphabetPairs;
	}

	ReflectLetter(letter) {
		return this.AlphabetPairs[letter.charCodeAt() - 65];
	}
};