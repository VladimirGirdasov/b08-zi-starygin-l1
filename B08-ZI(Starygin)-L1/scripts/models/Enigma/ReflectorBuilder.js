﻿class ReflectorBuilder {
	constructor() {
		this.ReflectorDictionary = {
			"reflector B":		["Y", "R", "U", "H", "Q", "S", "L", "D", "P", "X", "N", "G", "O", "K", "M", "I", "E", "B", "F", "Z", "C", "W", "V", "J", "A", "T"],
			"reflector C":		["F", "V", "P", "J", "I", "A", "O", "Y", "E", "D", "R", "Z", "X", "W", "G", "C", "T", "K", "U", "Q", "S", "B", "N", "M", "H", "L"],
			"reflector B Dünn": ["E", "N", "K", "Q", "A", "U", "Y", "W", "J", "I", "C", "O", "P", "B", "L", "M", "D", "X", "Z", "V", "F", "T", "H", "R", "G", "S"],
			"reflector C Dünn": ["R", "D", "O", "B", "J", "N", "T", "K", "V", "E", "H", "M", "L", "F", "C", "W", "Z", "A", "X", "G", "Y", "I", "P", "S", "U", "Q"]
		}
	}

	BuildReflector(reflectorName) {
		if (this.ReflectorDictionary[reflectorName] == undefined) {
			throw new Error(`Релектор с именем ${reflectorName} не существует`);
		}

		return new Reflector(reflectorName, this.ReflectorDictionary[reflectorName]);
	}

}