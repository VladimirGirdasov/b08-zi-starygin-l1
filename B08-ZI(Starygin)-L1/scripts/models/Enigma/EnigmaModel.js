﻿class EnigmaModel {
	constructor(textInput) {
		if (textInput == undefined)
			textInput = "";

		this.TextInput = textInput;
		const rotorBuilder = new RotorBuilder();
		const reflectorBuilder = new ReflectorBuilder();

		this.Reflector = reflectorBuilder.BuildReflector("reflector B");
		this.Rotors = [	rotorBuilder.BuildRotor("Rotor III"),
						rotorBuilder.BuildRotor("Rotor II"),
						rotorBuilder.BuildRotor("Rotor I")];

		this.Rotors[0].SelectRotorIndex("X".charCodeAt() - 65);
		this.Rotors[1].SelectRotorIndex("D".charCodeAt() - 65);
		this.Rotors[2].SelectRotorIndex("H".charCodeAt() - 65);
		
		this.Answer = this.CalcAnswer();
	}

	SetTextInput(text) {
		this.TextInput = text;
	}

	SetRotor(indexOfRotor, rotor) {
		if (rotor instanceof Rotor !== true) {
			throw new Error("setRotor получил на вход не ротор!");
		}

		if ((this.Rotors.count > indexOfRotor + 1) || (indexOfRotor < 0)) {
			throw new Error("Ротора с таким индексом не существует");
		}

		this.Rotors[indexOfRotor] = rotor;
	}

	SetReflector(reflector) {
		this.Reflector = reflector;
	}

	SetRotorSelectedLetter(indexOfRotor, letter) {
		this.Rotors[indexOfRotor].SelectRotorIndex(letter.charCodeAt() - 65);
	}

	SetReflector(reflector) {
		if (reflector instanceof Reflector !== true) {
			throw new Error("setReflecor получил на вход не рефлектор!");
		}

		this.Reflector = reflector;
	}

	_GetPreviousLetter(switchIndex) {
		const letter = String.fromCharCode(65 + 1 + switchIndex);

		if (letter.charCodeAt() > "Z".charCodeAt()) {
			return "A";
		} else {
			return letter;
		}
	}

	CalcAnswer() {
		this.Answer = "";

		// текущее положение роторов. Если оно дойдёт до Rotor.Switches до должен передвинуться соседний ротор
		let switches = [this.Rotors[0].SelectedRotorIndex,
						this.Rotors[1].SelectedRotorIndex,
						this.Rotors[2].SelectedRotorIndex];

		for (let i = 0; i < this.TextInput.length; i++) {

			console.log("=====================");
			console.log("Letter started: " + this.TextInput[i]);

			/////////////
			switches[0]++;
			if (switches[0] > 25) {
				switches[0] = 0;
			}

			// roll 1 100%
			if (this.Rotors[0].Switches.includes(String.fromCharCode(65 + switches[0]))) {
				switches[1]++;
				if (switches[1] > 25) {
					switches[1] = 0;
				}

				// analyze 2
				// roll 2 100%
				if (this.Rotors[1].Switches.includes(String.fromCharCode(65 + switches[1]))) {
					switches[2]++;
					if (switches[2] > 25) {
						switches[2] = 0;
					}
				}
					// roll 2 if 25 after trigger
				else {
					if (this.Rotors[2].Switches.includes(this._GetPreviousLetter(switches[2]))) {
						switches[2]++;
						if (switches[2] > 25) {
							switches[2] = 0;
						}
					}
				}
				// end analyze 2
			}
			// roll 1 if 25 after trigger
			else {
				if (this.Rotors[1].Switches.includes(this._GetPreviousLetter(switches[1]))) { // todo
					switches[1]++;
					if (switches[1] > 25) {
						switches[1] = 0;
					}

					// analyze 2
					// roll 2 100%
					if (this.Rotors[1].Switches.includes(String.fromCharCode(65 + switches[1]))) {
						switches[2]++;
						if (switches[2] > 25) {
							switches[2] = 0;
						}
					}
						// roll 2 if 25 after trigger
					else {
						if (this.Rotors[2].Switches.includes(this._GetPreviousLetter(switches[2]))) {
							switches[2]++;
							if (switches[2] > 25) {
								switches[2] = 0;
							}
						}
					}
					// end analyze 2
				}
			}

			console.log("switches = " + String.fromCharCode(65 + switches[0]) + ", " + String.fromCharCode(65 + switches[1]) + ", " + String.fromCharCode(65 + switches[2]));

			// Текущая буква
			let letter = this.TextInput[i];

			if (letter.charAt() < "A"[0] || letter.charAt() > "Z"[0]) {
				throw new Error(`Машина Энигма не могла работать с символом [${letter}]`);
			}

			for (let rotorNumber = 0; rotorNumber < this.Rotors.length; rotorNumber++) {
				letter = this.Rotors[rotorNumber].PassLetterThroughAndGetNextRotorIndex(letter, switches[rotorNumber] - this.Rotors[rotorNumber].SelectedRotorIndex);
			}

			letter = this.Reflector.ReflectLetter(letter);
			//console.log("Reflected letter is " + letter);

			var backDelta = letter.charCodeAt() - 65;

			for (let rotorNumber = this.Rotors.length - 1; rotorNumber >= 0; rotorNumber--) {
				letter = this.Rotors[rotorNumber].PassIndexThroughAndGetBackRotorIndex(letter, switches[rotorNumber] - this.Rotors[rotorNumber].SelectedRotorIndex);
			}

			this.Answer += letter.toString();
		}

		console.log("END ANSWER = " + this.Answer);
		return this.Answer;
	}
}