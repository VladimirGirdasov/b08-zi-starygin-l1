﻿class RotorBuilder {
	constructor() {
		this.RotorsDictionary = {
			"Rotor I":		{pairs: ["E", "K", "M", "F", "L", "G", "D", "Q", "V", "Z", "N", "T", "O", "W", "Y", "H", "X", "U", "S", "P", "A", "I", "B", "R", "C", "J"], switches: ["R"]},
			"Rotor II":		{pairs: ["A", "J", "D", "K", "S", "I", "R", "U", "X", "B", "L", "H", "W", "T", "M", "C", "Q", "G", "Z", "N", "P", "Y", "F", "V", "O", "E"], switches: ["F"]},
			"Rotor III":	{pairs: ["B", "D", "F", "H", "J", "L", "C", "P", "R", "T", "X", "V", "Z", "N", "Y", "E", "I", "W", "G", "A", "K", "M", "U", "S", "Q", "O"], switches: ["W"]},
			"Rotor IV":		{pairs: ["E", "S", "O", "V", "P", "Z", "J", "A", "Y", "Q", "U", "I", "R", "H", "X", "L", "N", "F", "T", "G", "K", "D", "C", "M", "W", "B"], switches: ["K"]},
			"Rotor V":		{pairs: ["V", "Z", "B", "R", "G", "I", "T", "Y", "U", "P", "S", "D", "N", "H", "L", "X", "A", "W", "M", "J", "Q", "O", "F", "E", "C", "K"], switches: ["A"]},
			"Rotor VI":		{pairs: ["J", "P", "G", "V", "O", "U", "M", "F", "Y", "Q", "B", "E", "N", "H", "Z", "R", "D", "K", "A", "S", "X", "L", "I", "C", "T", "W"], switches: ["A", "N"]},
			"Rotor VII":	{pairs: ["N", "Z", "J", "H", "G", "R", "C", "X", "M", "Y", "S", "W", "B", "O", "U", "F", "A", "I", "V", "L", "P", "E", "K", "Q", "D", "T"], switches: ["A", "N"]},
			"Rotor VIII":	{pairs: ["F", "K", "Q", "H", "T", "L", "X", "O", "C", "B", "J", "S", "P", "D", "Z", "R", "A", "M", "E", "W", "N", "I", "U", "Y", "G", "V"], switches: ["A", "N"]}
		}														 
	}

	BuildRotor(rotorName) {
		if (this.RotorsDictionary[rotorName] == undefined) {
			throw new Error(`Ротора с именем ${rotorName} не существует`);
		}

		return new Rotor(rotorName, this.RotorsDictionary[rotorName].pairs, this.RotorsDictionary[rotorName].switches);
	}
}