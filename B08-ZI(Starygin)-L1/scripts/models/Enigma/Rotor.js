﻿class Rotor {
	// alphabetPairs - ожидает массив(26) символов ключей на который отражается i-й символ англ. алфавита
	constructor(name, alphabetPairs, switches) {
		this.Name = name.toString();

		this.Switches = switches;
		
		if (alphabetPairs.length !== 26) {
			throw Error("Классический ротор должен состоять из 26 символов");
		}

		this.AlphabetPairs = {};

		for (let i = 0; i < 26; i ++) {
			this.AlphabetPairs[String.fromCharCode(i + 65)] = alphabetPairs[i];
		}

		this.SelectedRotorIndex = 0;
	}

	ReflectBack(letter) {
		return Object.keys(this.AlphabetPairs).find(key => this.AlphabetPairs[key] === letter);
	}

	SelectRotorIndex(index) {
		if (index >= 26) {
			throw new Error(`Ротор прокрутиться на значение больше чем 26, пришло значение${index}`);
		}

		this.SelectedRotorIndex = index;
	}

	_ApplyDelta(letter, delta, deltaRot) {
		let code = letter.charCodeAt() - 65;

		code += (delta + deltaRot);


		if (code > 25) {
			code = code % 26;
		}

		const ret = String.fromCharCode(code + 65);
		return ret;
	}

	_RemoveDelta(letter, delta, deltaRot) {
		let code = letter.charCodeAt() - 65;

		code -= (delta + deltaRot);

		if (code < 0) {
			code = (code + 1) % 26;
			const ret = String.fromCharCode("Z".charCodeAt() + code);
			return ret;
		}

		const ret = String.fromCharCode(code + 65);
		return ret;
	}

	GetSelectedLetter() {
		return String.fromCharCode(65 + this.SelectedRotorIndex);
	}

	// Проход к рефлектору
	PassLetterThroughAndGetNextRotorIndex(letter, delta) {
		letter = this._ApplyDelta(letter, delta, this.SelectedRotorIndex);

		//console.log("Rotor input letter is " + letter);

		const outLetter = this.AlphabetPairs[letter];

		//console.log("Rotor output letter is " + outLetter);

		return this._RemoveDelta(outLetter, delta, this.SelectedRotorIndex);
	}

	// Проход назад от рефлектора
	PassIndexThroughAndGetBackRotorIndex(letter, delta) {
		letter = this._ApplyDelta(letter, delta, this.SelectedRotorIndex);

		//console.log("Rotor input letter is " + letter);

		const outLetter = this.ReflectBack(letter);

		//console.log("Rotor output letter is " + outLetter);
		
		return this._RemoveDelta(outLetter, delta, this.SelectedRotorIndex);
	}
};