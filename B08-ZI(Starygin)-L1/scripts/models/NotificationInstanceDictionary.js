﻿class NotificationInstanceDictionary {
	constructor() {
		this.Dictionary = {};
	}

	Add(key) {
		this.Dictionary[key] = new NotificationInstance();
	}

	Get(key) {
		if (this.Dictionary[key] == undefined) {
			throw new Error(Resources.Messages.ErrorNotificationDictionaryGetTriedToAccessUnexistedResource);
		}
		return this.Dictionary[key];
	}

	ContainsKey(key) {
		return this.Dictionary[key] !== undefined;
	}

	Set(key, notificationInstance) {
		if (notificationInstance instanceof NotificationInstance === false) {
			throw new Error(Resources.Messages.ErrorNotificationDictionarySet_NotificationInstanceWrongType);
		}

		this.Dictionary[key] = notificationInstance;
		return this.Dictionary[key];
	}
};