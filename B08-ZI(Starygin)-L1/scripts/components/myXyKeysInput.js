﻿angular.module("myApp")
.component("myXyKeysInput", {
	bindings: {
		keysXInput: "=",
		keysYInput: "=",
		applyKeysXCallback: "&",
		applyKeysYCallback: "&"
	},
	templateUrl: "scripts/templates/_myXyKeysInput.html"
})