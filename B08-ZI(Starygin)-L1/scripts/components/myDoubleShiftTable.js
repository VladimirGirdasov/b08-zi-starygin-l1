﻿angular.module("myApp")
.component("myDoubleShiftTable", {
	bindings: {
		table: "<",
		animate: "=",
		animateButtonDisabled: "="
	},
	templateUrl: "scripts/templates/_myDoubleShiftTable.html"
})