﻿angular.module("myApp")
.component("myXyCypherKeys", {
	bindings: {
		keysX: "=",
		keysY: "=",
		shuffleKeysXCallback: "&",
		shuffleKeysYCallback: "&",
		copyKeysToClipboard: "&"
	},
	templateUrl: "scripts/templates/_myXyCypherKeys.html"
})