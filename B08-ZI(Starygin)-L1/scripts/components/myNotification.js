﻿angular.module("myApp")
.component("myNotification", {
	bindings: {
		message: "=",
		imgAlt: "=",
		myStyle: "=",
		imgPath: "=",
		hideAction: "=",
		key: "="
	},
	templateUrl: "scripts/templates/_myNotification.html"
})