﻿angular.module("myApp")
.component("myCypherAnswer", {
	bindings: {
		answer: "=",
		copyAnswerToClipboard: "&",
		title: "@"
	},
	templateUrl: "scripts/templates/_myCypherAnswer.html"
})