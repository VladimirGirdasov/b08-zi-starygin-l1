﻿angular.module("myApp")
.component("myInputBlock", {
	bindings: {
		textInput: "=",
		messageToUser: "<"
	},
	templateUrl: "scripts/templates/_myInputBlock.html"
})