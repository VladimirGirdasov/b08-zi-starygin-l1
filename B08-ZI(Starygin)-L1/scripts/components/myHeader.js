﻿angular.module("myApp")
.component("myHeader", {
	bindings: {
		titles: "=",
		author: "=",
		selectedLabIndex: "=",
		setLabIndex: "="
	},
	templateUrl: "scripts/templates/_header.html"
})