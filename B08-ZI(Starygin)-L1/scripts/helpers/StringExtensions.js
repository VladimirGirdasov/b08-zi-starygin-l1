﻿function chunkString(str, length) {
	return str.match(new RegExp('.{1,' + length + '}', 'g'));
}

String.prototype.replaceAt = function (index, character) {
	return this.substr(0, index) + character + this.substr(index + character.length);
}