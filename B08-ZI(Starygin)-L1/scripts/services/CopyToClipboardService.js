﻿myApp.service("copyToClipboardService",
	function ($rootScope) {
		
		//Event: Copy success
		const eventCopySuccess = function (data) {
			$rootScope.$broadcast(Resources.Events.CopyToClipboardSuccess, data);
		};

		//Event: Copy error
		const eventCopyError = function (data) {
			$rootScope.$broadcast(Resources.Events.CopyToClipboardError, data);
		};

    	function copyToClipboard(text) {
    		const textArea = document.createElement("textarea");

    		textArea.style.position = "fixed";
    		textArea.style.top = 0;
    		textArea.style.left = 0;
    		textArea.style.width = "2em";
    		textArea.style.height = "2em";
    		textArea.style.padding = 0;
    		textArea.style.border = "none";
    		textArea.style.outline = "none";
    		textArea.style.boxShadow = "none";
    		textArea.style.background = "transparent";
    		textArea.value = text;

    		document.body.appendChild(textArea);

    		textArea.select();

    		try {
    			const successful = document.execCommand("copy");
    			const msg = successful ? "successful" : "unsuccessful";
    			console.log(`Copying text command was ${msg}`);
			    eventCopySuccess();
		    } catch (err) {
		    	alert("Oops, unable to copy");
			    eventCopyError();
		    }

    		document.body.removeChild(textArea);
    	}

    	return {
    		CopyToClipboard: copyToClipboard
    	};
    }
);