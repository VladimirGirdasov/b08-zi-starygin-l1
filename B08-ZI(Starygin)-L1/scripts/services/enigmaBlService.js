﻿myApp.service("enigmaBlService",
	function (arrayShuffleService, $rootScope, $interval) {

		var model = new EnigmaModel("PUTSOMEDATAHERE");

		var answer = model.CalcAnswer();

		return {
			GetTextInput: function() {
				return model.TextInput;
			},
			SetTextInput: function(text) {
				model.SetTextInput(text);
				this.answer = model.CalcAnswer();
			},
			Answer: function() {
				return this.answer;
			},
			GetRotorsDictionary: function() {
				const rotorBuilder = new RotorBuilder();

				const keys = Object.keys(rotorBuilder.RotorsDictionary);
				return keys;
			},
			GetReflectorsDictionary: function () {
				const reflectorBuilder = new ReflectorBuilder();

				const keys = Object.keys(reflectorBuilder.ReflectorDictionary);
				return keys;
			},
			SetRotor: function(indexOfRotor, rotorName) {
				const rotorBuilder = new RotorBuilder();

				model.SetRotor(indexOfRotor, rotorBuilder.BuildRotor(rotorName));
			},
			SetReflector: function(reflectorName) {
				const reflectorBuilder = new ReflectorBuilder();

				model.SetReflector(reflectorBuilder.BuildReflector(reflectorName));
			},
			GetRotorsStates: function() {
				return model.Rotors;
			},
			GetReflectorState: function () {
				return model.Reflector;
			},
			SetRotorSelectedLetter: function(indexOfRotor,  letter) {
				model.SetRotorSelectedLetter(indexOfRotor, letter);
			}
		};
	}
);