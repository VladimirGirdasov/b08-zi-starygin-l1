﻿myApp.service("doubleShiftBlService",
	function(arrayShuffleService, $rootScope, $interval) {

		function closeAbsoluteObject(jqObj) {
			return function() {
				jqObj.css({
					position: "initial",
					color: "initial",
					fontSize: "initial"
				});
			};
		}

		function swapViewTableLines(cellAxisIndexA, cellAxisIndexB, countCells, boolHorizontalAxis) {

			// Always its a 1st slide, not an error :)
			if (boolHorizontalAxis == undefined)
				return;

			for (let indexCell = 0; indexCell < countCells; indexCell++) {

				let aCell, bCell;

				if (!boolHorizontalAxis) {
					aCell = $(`#MyTable tr:eq(${indexCell}) td:eq(${cellAxisIndexA}) div:nth-child(1):eq(0)`);
					bCell = $(`#MyTable tr:eq(${indexCell}) td:eq(${cellAxisIndexB}) div:nth-child(1):eq(0)`);
				} else {
					aCell = $(`#MyTable tr:eq(${cellAxisIndexA}) td:eq(${indexCell}) div:nth-child(1):eq(0)`);
					bCell = $(`#MyTable tr:eq(${cellAxisIndexB}) td:eq(${indexCell}) div:nth-child(1):eq(0)`);
				}

				const aOffsetTop = aCell.offset().top;
				const aOffsetLeft = aCell.offset().left;
				const bOffsetTop = bCell.offset().top;
				const bOffsetLeft = bCell.offset().left;

				aCell.css({
					position: "absolute",
					top: aOffsetTop,
					left: aOffsetLeft,
					color: "rgb(92, 184, 92)",
					fontSize: "x-large"
				});

				bCell.css({
					position: "absolute",
					top: bOffsetTop,
					left: bOffsetLeft,
					color: "rgb(91, 192, 222)",
					fontSize: "x-large"
				});

				aCell.animate({
						top: bOffsetTop,
						left: bOffsetLeft
					},
					900,
					closeAbsoluteObject(aCell)
				);

				bCell.animate({
						top: aOffsetTop,
						left: aOffsetLeft
					},
					900,
					closeAbsoluteObject(bCell)
				);
			}
		}

		var model = new DoubleShiftModel("");
		var decypherMod = false;
		var animationQueue = getAnimationQueueAccordingToMod();
		

		//Event: pass animation slide to observers
		var eventPassAnimationSlide = function (data) {
			$rootScope.$broadcast(Resources.Events.PassAnimationSlide, data);
		};

		// Observer: Copy to clipboard error
		$rootScope.$on(Resources.Events.DecypherModSwitched,
			function (event, data) {
				decypherMod = data;
			}
		);

		// Pass event animation slide each quantum of time
		$interval(
			function() {
				if (animationQueue.length === 0) {
					return;
				} else {
					const animationElement = animationQueue.shift();
					swapViewTableLines(animationElement.CellAxisIndexA,
						animationElement.CellAxisIndexB,
						model.GridSize + 1,
						animationElement.BoolHorizontalAxis);
					eventPassAnimationSlide(animationElement.ViewTableSlide);
				}
			},
			1000);

		function getAnimationQueueAccordingToMod() {
			let queue;
			if (decypherMod === true) {
				queue = model.getAnimationQueueSourceText();
			} else {
				queue = model.getAnimationQueue();
			}

			return queue;
		}

		return {
			GetTextInput: function() {
				return model.TextInput;
			},
			SetTextInput: function(text) {
				model = new DoubleShiftModel(text);
				animationQueue = getAnimationQueueAccordingToMod();
			},

			GetKeysX: function() {
				return model.KeysX;
			},
			GetKeysY: function() {
				return model.KeysY;
			},

			ShuffleKeysX: function() {
				model.KeysX = model.generateKeys();
				animationQueue = getAnimationQueueAccordingToMod();
				return model.KeysX;
			},
			ShuffleKeysY: function () {
				model.KeysY = model.generateKeys();
				animationQueue = getAnimationQueueAccordingToMod();
				return model.KeysY;
			},

			ApplyKeysX: function (data) {
				model.KeysX = data;
				animationQueue = getAnimationQueueAccordingToMod();
				return model.KeysX;
			},
			ApplyKeysY: function (data) {
				model.KeysY = data;
				animationQueue = getAnimationQueueAccordingToMod();
				return model.KeysY;
			},

			GetGridSize: function() {
				model.GridSize = model.calcGridSize();
				return model.GridSize;
			},

			GetViewTable: function() {
				return model.generateViewTable(model.TextInput, model.KeysX, model.KeysY);
			},

			GetAnswer: function () {
				return model.calcAnswer();
			},

			GetAnswerSourceText: function () {
				return model.calcAnswerSourceText();
			},

			ReplayAnimation: function () {
				animationQueue = getAnimationQueueAccordingToMod();
			}
		};
	}
);