﻿myApp.service("notificationsService",
    function () {

    	const styles = {
    		// background 0000000[xx]
    		bcLoading: 0x1,
    		bcSuccess: 0x2,
    		bcFail: 0x3,
    		// default. Shall not pass to view through ng-if
    		Hide: 0x0,
    		// use it to extract background color from whole style
    		bcExtract: 0x3,

    		// bottom: 000000[x]00
    		// 55%
    		Centralize: 0x4,

    		// position: 00000[x]000
    		// fixed;
    		positionFixed: 0x8
    	};

    	const mask2Style = {
    		0x1: "ntf-loading",
    		0x2: "ntf-success",
    		0x3: "ntf-fail",
    		0x4: "ntf-centralize",
    		0x8: "ntf-fixed"
    	};

    	const imgSetEnums = {
    		OK: { imgPath: "images\\ok.png", imgAlt: "ok.png" },
    		ERROR: { imgPath: "images\\no.png", imgAlt: "no.png" },
    		WAIT: { imgPath: "images\\loading.gif", imgAlt: "loading.gif" }
    	};

    	var notificationInstanceDictionary = new NotificationInstanceDictionary();

    	var registerNotification = function (notificationElementId) {
    		if (notificationInstanceDictionary.ContainsKey(notificationElementId) === false) {
    			notificationInstanceDictionary.Add(notificationElementId);

    			var element = angular.element(document.querySelector(notificationElementId));
    			element.addClass("ntf-default");
    		}

    		return notificationInstanceDictionary.Get(notificationElementId);
    	}

    	const hideNotification = function (key) {
    		var currentInstance = notificationInstanceDictionary.Get(key);

    		if (currentInstance == undefined || currentInstance instanceof NotificationInstance === false) {
    			throw new Error(Resources.Messages.ErrorNotificationServiceTriedToModifyUnregisteredNotification);
    		}

    		currentInstance.Style ^= currentInstance.Style;
    		currentInstance.Message = null;

    		return notificationInstanceDictionary.Set(key, currentInstance);
    	};

    	var resolveStyleDiffs = function (curStyle, newStyle, selector) {
    		var element = angular.element(document.querySelector(selector));

    		// resolve background diff
    		if ((curStyle & styles.bcExtract) !== (newStyle & styles.bcExtract)) {
    			element.removeClass(mask2Style[curStyle & styles.bcExtract]);
    			element.addClass(mask2Style[newStyle & styles.bcExtract]);
    		}

    		// resolve position diff
    		if ((curStyle & styles.Centralize) !== (newStyle & styles.Centralize)) {
    			if ((curStyle & styles.Centralize) === styles.Centralize) {
    				element.removeClass(mask2Style[curStyle & styles.Centralize]);
    			} else {
    				element.addClass(mask2Style[newStyle & styles.Centralize]);
    			}
    		}

    		// resolve bottom diff
    		if ((curStyle & styles.positionFixed) !== (newStyle & styles.positionFixed)) {
    			if ((curStyle & styles.positionFixed) === styles.positionFixed) {
    				element.removeClass(mask2Style[curStyle & styles.positionFixed]);
    			} else {
    				element.addClass(mask2Style[newStyle & styles.positionFixed]);
    			}
    		}

    		return newStyle;
    	}

    	var createNewStyle = function (bcColor, bOnCenter, bFixed, currentInstance, elementSelector) {
    		var newStyle = bcColor;
    		if (bOnCenter) {
    			newStyle |= styles.Centralize;
    		}
    		if (bFixed) {
    			newStyle |= styles.positionFixed;
    		}

    		resolveStyleDiffs(currentInstance.Style, newStyle, elementSelector);

    		return newStyle;
    	}

    	const setFailNotification = function (key, message, bOnCenter, bFixed) {
    		var currentInstance = notificationInstanceDictionary.Get(key);

    		if (currentInstance == undefined || currentInstance instanceof NotificationInstance === false) {
    			throw new Error(styles.bcFail, Resources.Messages.ErrorNotificationServiceTriedToModifyUnregisteredNotification);
    		}

    		currentInstance.Style = createNewStyle(styles.bcFail, bOnCenter, bFixed, currentInstance, key);
    		currentInstance.ImgPath = imgSetEnums.ERROR.imgPath;
    		currentInstance.ImgAlt = imgSetEnums.ERROR.imgAlt;
    		currentInstance.Message = message;

    		return notificationInstanceDictionary.Set(key, currentInstance);
    	};

    	const setLoadingNotification = function (key, message, bOnCenter, bFixed) {
    		var currentInstance = notificationInstanceDictionary.Get(key);

    		if (currentInstance == undefined || currentInstance instanceof NotificationInstance === false) {
    			throw new Error(Resources.Messages.ErrorNotificationServiceTriedToModifyUnregisteredNotification);
    		}

    		currentInstance.Style = createNewStyle(styles.bcLoading, bOnCenter, bFixed, currentInstance, key);
    		currentInstance.ImgPath = imgSetEnums.WAIT.imgPath;
    		currentInstance.ImgAlt = imgSetEnums.WAIT.imgAlt;
    		currentInstance.Message = message;

    		return notificationInstanceDictionary.Set(key, currentInstance);
    	};

    	const setSuccessNotification = function (key, message, bOnCenter, bFixed) {
    		var currentInstance = notificationInstanceDictionary.Get(key);

    		if (currentInstance == undefined || currentInstance instanceof NotificationInstance === false) {
    			throw new Error(Resources.Messages.ErrorNotificationServiceTriedToModifyUnregisteredNotification);
    		}

    		currentInstance.Style = createNewStyle(styles.bcSuccess, bOnCenter, bFixed, currentInstance, key);
    		currentInstance.ImgPath = imgSetEnums.OK.imgPath;
    		currentInstance.ImgAlt = imgSetEnums.OK.imgAlt;
    		currentInstance.Message = message;

    		return notificationInstanceDictionary.Set(key, currentInstance);
    	};

    	const getInstance = function (key) {
    		return notificationInstanceDictionary.Get(key);
    	};

    	return {
    		Register: registerNotification,
    		Hide: hideNotification,
    		Fail: setFailNotification,
    		Loading: setLoadingNotification,
    		Success: setSuccessNotification,
    		GetInstance: getInstance
    	};
    }
);