﻿var Resources = {
	Errors: {
		KeysLengthsAreNotEqual: "Lengths of Cypher keys are not equeal",
		KeysTypesIsNotArray: "Array keys should type of array",
		GridSizeShouldBeEqualKeysLength: "Grid size isn't equal keys-array lengths",
		ErrorNotificationDictionaryGetTriedToAccessUnexistedResource: "NotificationInstanceDictionary.Get tried to access to an unexisted notification. Fix it",
	ErrorNotificationDictionarySet_NotificationInstanceWrongType: "NotificationInstanceDictionary.Set: Second parameter should be type of NotificationInstance",
	ErrorNotificationServiceTriedToModifyUnregisteredNotification: "NotificationService.set~Notification: Cant set state, because previous state doesn't exists or corrupted"
	},
	Events: {
		KeysShuffled: 1,
		AnimationColumnDone: 2,
		AnimationCellsSwap: 3,
		PassAnimationSlide: 4,
		CopyToClipboardError: 5,
		CopyToClipboardSuccess: 6,
		DecypherModSwitched: 7
	},
	Selector: {
		ButtonCopyKeysToClipboard: "#btnCopyKeys",
		NotificationGlobal: "#notification_global"
	},
	Messages: {
		CopyToClipboardError: "Ошибка копирования! Возможно браузер не поддерживается",
		CopyToClipboardSuccess: "Копирование прошло успешно :)",
		KeysIs: "Ключами являются числа от 1 с шагом 1 разделенные запятыми"
	}
};