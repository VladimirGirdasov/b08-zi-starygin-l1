﻿// Body under fixed header with dynamic height
var divHeight = $("header").height();

const constOffset = 20;

$(".content").css("padding-top", divHeight + constOffset + "px");

$(window)
	.resize(function() {
		const divHeight = $("header").height();
		$(".content").css("padding-top", divHeight + constOffset + "px");
	});

