﻿myApp.controller("doubleShiftCtrl",
	function doubleShiftCtrl($scope, $rootScope, $route, doubleShiftBlService, $timeout, copyToClipboardService, notificationsService) {

		$scope.model = {
			MessageToUserOnInput: "Исходный текст: ",
			TextInput: doubleShiftBlService.GetTextInput(),
			KeysX: doubleShiftBlService.GetKeysX(),
			KeysY: doubleShiftBlService.GetKeysY(),
			KeysXInput: "",
			KeysYInput: "",
			GridSize: doubleShiftBlService.GetGridSize(),
			ViewTable: [],
			// Результат шифрации
			Answer: "",
			// Результат дешифрации
			AnswerSourceText: "",
			Animate: doubleShiftBlService.ReplayAnimation,
			Notification: {
				Global: {
					Key: Resources.Selector.NotificationGlobal,
					Instance: notificationsService.Register(Resources.Selector.NotificationGlobal)
				},
				Hide: notificationsService.Hide
			},
			DeCypherMod: false
	};

		// Observer: New Animation slide
		$rootScope.$on(Resources.Events.PassAnimationSlide,
			function (event, data) {
				$timeout(function() {
						$scope.model.ViewTable = data;
					},
					900);
			}
		);

		// Observer: Copy to clipboard success
		$rootScope.$on(Resources.Events.CopyToClipboardSuccess,
			function (event, data) {
				$scope.model.Notification.Global.Instance = notificationsService.Success($scope.model.Notification.Global.Key, Resources.Messages.CopyToClipboardSuccess, false, true);
			}
		);

		// Observer: Copy to clipboard error
		$rootScope.$on(Resources.Events.CopyToClipboardError,
			function (event, data) {
				$scope.model.Notification.Global.Instance = notificationsService.Fail($scope.model.Notification.Global.Key, Resources.Messages.CopyToClipboardError, false, true);
			}
		);

		//Event: pass animation slide to observers
		var eventDecypherModSwithced = function (data) {
			$rootScope.$broadcast(Resources.Events.DecypherModSwitched, data);
		};

		// Button copy keys to clipboard
		$scope.model.CopyKeysToClipboard = function () {
			const text = `Keys X axis: [${doubleShiftBlService.GetKeysX()}], Keys Y axis: [${doubleShiftBlService.GetKeysY()}]`;
			copyToClipboardService.CopyToClipboard(text);
		}

		// Button copy answer to clipboard
		$scope.model.CopyAnswerToClipboard = function () {
			const text = `Cypher result: [${doubleShiftBlService.GetAnswer()}] (trim brackets!)`;
			copyToClipboardService.CopyToClipboard(text);
		}

		$scope.$watch("model.TextInput", function (newValue, oldValue) {
			doubleShiftBlService.SetTextInput(newValue);
			$scope.model.KeysX = doubleShiftBlService.GetKeysX();
			$scope.model.KeysY = doubleShiftBlService.GetKeysY();
			$scope.model.GridSize = doubleShiftBlService.GetGridSize();

			$scope.model.ViewTable = doubleShiftBlService.GetViewTable();
			addGlyphIconLockImage();

			$scope.model.Answer = doubleShiftBlService.GetAnswer();
			$scope.model.AnswerSourceText = doubleShiftBlService.GetAnswerSourceText();
		});

		$scope.model.ShuffleKeysX = function() {
			$scope.model.KeysX = doubleShiftBlService.ShuffleKeysX();

			$scope.model.ViewTable = doubleShiftBlService.GetViewTable();
			addGlyphIconLockImage();

			$scope.model.Answer = doubleShiftBlService.GetAnswer();
			$scope.model.AnswerSourceText = doubleShiftBlService.GetAnswerSourceText();
		};

		$scope.model.ShuffleKeysY = function() {
			$scope.model.KeysY = doubleShiftBlService.ShuffleKeysY();

			$scope.model.ViewTable = doubleShiftBlService.GetViewTable();
			addGlyphIconLockImage();

			$scope.model.Answer = doubleShiftBlService.GetAnswer();
			$scope.model.AnswerSourceText = doubleShiftBlService.GetAnswerSourceText();
		};

		$('#toggle-mod')
			.change(function() {
				$scope.model.DeCypherMod = !$scope.model.DeCypherMod;

				eventDecypherModSwithced($scope.model.DeCypherMod);

				doubleShiftBlService.ReplayAnimation();
			});

		$scope.model.ApplyKeysX = function () {
			let lengthNeed = doubleShiftBlService.GetGridSize();

			let keysPossible = $scope.model.KeysXInput.split(",");

			for (let i =0;i< lengthNeed;i++)

			if (keysPossible.length !== lengthNeed) {
				$scope.model.Notification.Global.Instance = notificationsService.Fail($scope.model.Notification.Global.Key, "Не корректное количество ключей", false, true);
				return;
			} 

			let arr = [];

			for (let i = 0; i < lengthNeed; i++) {
				arr.push(keysPossible[i] - 0);
			}

			let arrSorted = arr.slice(0).sort((a, b) => a - b);

			for (let i = 1; i < lengthNeed + 1; i++) {
				if (arrSorted[i-1] !== i) {
					$scope.model.Notification.Global.Instance = notificationsService.Fail($scope.model.Notification.Global.Key, Resources.Messages.KeysIs, false, true);
					return;
				}
			}

			doubleShiftBlService.ApplyKeysX(arr);
			$scope.model.KeysX = doubleShiftBlService.GetKeysX();
			$scope.model.Notification.Global.Instance = notificationsService.Success($scope.model.Notification.Global.Key, "Ок, ключи приняты!", false, true);

			$scope.model.ViewTable = doubleShiftBlService.GetViewTable();
			addGlyphIconLockImage();

			$scope.model.Answer = doubleShiftBlService.GetAnswer();
			$scope.model.AnswerSourceText = doubleShiftBlService.GetAnswerSourceText();
		}

		$scope.model.ApplyKeysY = function () {
			let lengthNeed = doubleShiftBlService.GetGridSize();

			let keysPossible = $scope.model.KeysYInput.split(",");

			for (let i = 0; i < lengthNeed; i++)

				if (keysPossible.length !== lengthNeed) {
					$scope.model.Notification.Global.Instance = notificationsService.Fail($scope.model.Notification.Global.Key, "Не корректное количество ключей", false, true);
					return;
				}

			let arr = [];

			for (let i = 0; i < lengthNeed; i++) {
				arr.push(keysPossible[i] - 0);
			}

			let arrSorted = arr.slice(0).sort((a, b) => a - b);

			for (let i = 1; i < lengthNeed + 1; i++) {
				if (arrSorted[i - 1] !== i) {
					$scope.model.Notification.Global.Instance = notificationsService.Fail($scope.model.Notification.Global.Key, Resources.Messages.KeysIs, false, true);
					return;
				}
			}

			doubleShiftBlService.ApplyKeysY(arr);
			$scope.model.KeysY = doubleShiftBlService.GetKeysY();
			$scope.model.Notification.Global.Instance = notificationsService.Success($scope.model.Notification.Global.Key, "Ок, ключи приняты!", false, true);

			$scope.model.ViewTable = doubleShiftBlService.GetViewTable();
			addGlyphIconLockImage();

			$scope.model.Answer = doubleShiftBlService.GetAnswer();
			$scope.model.AnswerSourceText = doubleShiftBlService.GetAnswerSourceText();
		}

		// Add glyphicon lock to viewTable
		function addGlyphIconLockImage() {
			$("#MyTable tr:first-child td:first-child")
				.html("<span class=\"glyphicon glyphicon-lock\" style=\"opacity: 0.5;\" aria-hidden=\"true\"></span>");
		}
	});