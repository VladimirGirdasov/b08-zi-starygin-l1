﻿myApp.controller("enigmaCtrl",
	function doubleShiftCtrl($scope, $rootScope, $route, enigmaBlService, $timeout, copyToClipboardService, notificationsService) {

		$scope.model = {
			MessageToUserOnInput: "Ввод сюда: ",
			TextInput: enigmaBlService.GetTextInput(),
			// Результат шифрации
			Answer: enigmaBlService.Answer,
			Notification: {
				Global: {
					Key: Resources.Selector.NotificationGlobal,
					Instance: notificationsService.Register(Resources.Selector.NotificationGlobal)
				},
				Hide: notificationsService.Hide
			},
			RotorsStates: enigmaBlService.GetRotorsStates,
			ReflectorState: enigmaBlService.GetReflectorState,
			GetRotorsDictionary: enigmaBlService.GetRotorsDictionary,
			GetReflectorsDictionary: enigmaBlService.GetReflectorsDictionary,
			Alphabet: [
				"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "L", "M", "N", "O", "P",
				"R", "S", "T", "V", "U", "W", "X", "Y", "Z"
			]
		};

		$scope.$watch("model.TextInput", function (newValue, oldValue) {
			newValue = newValue.toUpperCase();
			newValue = newValue.replace(/[^A-Z]/g, '');

			$scope.model.TextInput = newValue;

			enigmaBlService.SetTextInput(newValue);
			$scope.model.Answer = enigmaBlService.Answer();
		});

		$scope.model.UpdateSelectedLetter = function(rotorIndex, letter) {

			enigmaBlService.SetRotorSelectedLetter(rotorIndex, letter);
			enigmaBlService.SetTextInput($scope.model.TextInput);
			$scope.model.Answer = enigmaBlService.Answer();
		};

		$scope.model.UpdateRotor = function (indexOfRotor, rotorName) {
			enigmaBlService.SetRotor(indexOfRotor, rotorName);
			enigmaBlService.SetTextInput($scope.model.TextInput);
			$scope.model.Answer = enigmaBlService.Answer();
		};

		$scope.model.UpdateReflector = function(reflectorName) {
			enigmaBlService.SetReflector(reflectorName);
			enigmaBlService.SetTextInput($scope.model.TextInput);
			$scope.model.Answer = enigmaBlService.Answer();
		}
	});